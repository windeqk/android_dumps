#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:34a5237959e5b0bdcb86eab53aee515ca8c2bf4d; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:af27081aafd3333842f881de6f9e7cdc6fc4a167 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:34a5237959e5b0bdcb86eab53aee515ca8c2bf4d && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
